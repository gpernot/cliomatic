import os
import pkgutil
from pathlib import Path

from jinja2 import Environment, select_autoescape

from cliomatic.exceptions import CliomaticError


def init_repository(
    directory: Path,
    package_name: str,
    module_name: str | None = None,
    author: str | None = None,
):
    if module_name is None:
        module_name = package_name

    templates = [
        ("pyproject.toml.j2", Path("pyproject.toml")),
        ("tox.ini.j2", Path("tox.ini")),
        ("api.py.j2", Path(package_name) / "example" / "api.py"),
        ("empty.j2", Path(package_name) / "example" / "__init__.py"),
        ("main.init.py.j2", Path(package_name) / "__init__.py"),
        ("cli.py.j2", Path(package_name) / "cli.py"),
        ("command.py.j2", Path(package_name) / "example" / "command.py"),
        ("usage.md.j2", Path("doc") / "usage.md"),
    ]
    if os.path.exists(directory):
        raise CliomaticError(f"Directory exists: {os.fspath(directory)}")

    j2env = Environment(autoescape=select_autoescape(default_for_string=False))

    context = {
        "package_name": package_name,
        "module_name": module_name or package_name,
        "author": author,
    }

    for source, destination in templates:
        resource = pkgutil.get_data(
            "cliomatic_cli.init", str(Path("templates") / source)
        )
        if resource is None:
            raise CliomaticError(f"Can't load resource {source}")
        template = j2env.from_string(resource.decode("utf8"))
        (directory / destination.parent).mkdir(parents=True, exist_ok=True)
        with open(directory / destination, "w") as f:
            f.write(template.render(context))
