import click

from .api import init_repository


@click.command("init")
@click.argument("package", required=True)
@click.option(
    "--module", required=False, help="Python module name, if different from PACKAGE"
)
@click.option("--author", required=False, help="Package author and email")
@click.option(
    "--directory",
    "-d",
    type=click.Path(),
    help="Target directory for generated files.",
    default=None,
)
def cli(package, module, author, directory):
    """Initialise a new CLI program named PACKAGE"""
    if not directory:
        directory = package
    init_repository(directory, package, module, author)


def init():
    pass
