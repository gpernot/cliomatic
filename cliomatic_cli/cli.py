import importlib.metadata

from cliomatic import cli as _cli  # type: ignore

cli = _cli.make_cli(
    project_name="cliomatic",
    module_name="cliomatic_cli",
    version=importlib.metadata.version("cliomatic"),
)
