import pytest


class FakeSecretStorage:
    def create_item(*args, **kwargs):
        pass


class FakeSecretStorageItem:
    def __init__(self, key, secret):
        self.key = key
        self.secret = secret

    def get_attributes(self):
        return {"key": self.key}

    def get_secret(self):
        return self.secret


@pytest.fixture(autouse=True)
def mock_secretstorage(monkeypatch):
    monkeypatch.setattr("secretstorage.dbus_init", lambda: None)
    monkeypatch.setattr(
        "secretstorage.collection.get_default_collection",
        lambda conn: FakeSecretStorage(),
    )
    monkeypatch.setattr(
        "secretstorage.collection.search_items",
        lambda conn, attr: [
            FakeSecretStorageItem("icinga.user", b"username"),
            FakeSecretStorageItem("icinga.password", b"thepassword"),
            FakeSecretStorageItem("icinga.password", b"thepassword"),
            FakeSecretStorageItem("ldap.DEFAULT.bind_dn", b"uname"),
            FakeSecretStorageItem("ldap.DEFAULT.bind_password", b"s3cret"),
            FakeSecretStorageItem(
                "ldap.nbs.bind_dn", b"uid=first.name,ou=users,dc=example,dc=com"
            ),
            FakeSecretStorageItem("ldap.nbs.bind_password", b"sosecret"),
            FakeSecretStorageItem("foo.bar.baz", b"s3cret"),
            FakeSecretStorageItem("foo.bar.quux", b"verysecret"),
        ],
    )
