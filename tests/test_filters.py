from dataclasses import dataclass
from datetime import date

import pytest

from cliomatic.filtering import filter_match, parse_filter
from cliomatic.response import APIResponse


@dataclass
class DummyResponse(APIResponse):
    a_bool: bool
    an_int: int
    a_str: str
    a_date: date


class TestFilters:
    data = [
        DummyResponse(True, 0, "zero", date.fromisoformat("2001-06-22")),
        DummyResponse(False, 1, "one", date.fromisoformat("2004-10-20")),
        DummyResponse(True, 2, "two", date.fromisoformat("1976-11-30")),
    ]

    def test_parser(self):
        bool_filter = parse_filter("a_bool=True")
        assert bool_filter.fieldname == "a_bool" and bool_filter.operand == "True"
        int_filter = parse_filter("an_int>=9")
        assert int_filter.fieldname == "an_int" and int_filter.operand == "9"
        str_filter = parse_filter("a_str~=.*o")
        assert str_filter.fieldname == "a_str" and str_filter.operand == ".*o"

    @pytest.mark.parametrize("test_case", data)
    def test_filter(self, test_case):
        bool_filter = parse_filter("a_bool=True")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is True)

        bool_filter = parse_filter("a_bool=ON")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is True)

        bool_filter = parse_filter("a_bool=yEs")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is True)

        bool_filter = parse_filter("a_bool!=yEs")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is False)

        bool_filter = parse_filter("a_bool!=False")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is True)

        bool_filter = parse_filter("a_bool=False")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is False)

        bool_filter = parse_filter("a_bool!=True")
        assert filter_match(test_case, [bool_filter]) == (test_case.a_bool is False)

        int_filter = parse_filter("an_int==1")
        assert filter_match(test_case, [int_filter]) == (test_case.an_int == 1)

        int_filter = parse_filter("an_int<=1")
        assert filter_match(test_case, [int_filter]) == (test_case.an_int <= 1)

        int_filter = parse_filter("an_int<1")
        assert filter_match(test_case, [int_filter]) == (test_case.an_int < 1)

        int_filter = parse_filter("an_int>=1")
        assert filter_match(test_case, [int_filter]) == (test_case.an_int >= 1)

        int_filter = parse_filter("an_int>1")
        assert filter_match(test_case, [int_filter]) == (test_case.an_int > 1)

        str_filter = parse_filter("a_str=one")
        assert filter_match(test_case, [str_filter]) == (test_case.a_str == "one")

        str_filter = parse_filter("a_str!=one")
        assert filter_match(test_case, [str_filter]) == (test_case.a_str != "one")

        date_filter = parse_filter("a_date>=2004-10-20")
        assert filter_match(test_case, [date_filter]) == (
            test_case.a_date >= date.fromisoformat("2004-10-20")
        )

        filters = [
            parse_filter("a_bool=True"),
            parse_filter("an_int<1"),
            parse_filter("a_str!=foo"),
            parse_filter("a_date>2000-01-01"),
        ]
        assert filter_match(test_case, filters) == (test_case.a_str == "zero")
