import builtins
import os

from cliomatic.config import load_defaults

BASEDIR = os.path.normpath(os.path.dirname(__file__))

builtins_open = builtins.open


class TestConfig:
    @staticmethod
    def _mock_open(filename: str, *args, **kwargs):
        if os.path.basename(filename) == "config-base.toml":
            return builtins_open(
                os.path.join(BASEDIR, "fixtures", "config-base.toml"), "rb"
            )
        else:
            raise ValueError

    def test_foo(self, monkeypatch):
        with monkeypatch.context() as m:
            m.setattr(builtins, "open", self._mock_open)
            res = load_defaults("config-base")
        assert res["default_map"]["icinga"]["user"] == "username"
        assert res["obj"].config.ldap["DEFAULT"]["bind_password"] == "s3cret"
        assert res["obj"].config.ldap.DEFAULT.bind_password == "s3cret"
