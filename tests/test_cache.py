import json
from dataclasses import dataclass

from cliomatic.cache import Cache, cache
from cliomatic.response import APIGenerator, APIResponse
from cliomatic.tests import init_cache  # noqa: F401


@dataclass
class APITest(APIResponse):
    foo: str
    bar: bool
    baz: float


class TestCache:
    # dbm: dict = {}

    @cache()
    def echoer(self, data: list[str]) -> APIGenerator[APITest]:
        for e in data:
            yield APITest(foo=e, bar=True, baz=len(e))

    def test_cache(self):
        assert len(Cache.db) == 0

        data = "Never go against a Sicilian when death is on the line".split()
        res1 = list(self.echoer(data))

        assert len(Cache.db) == 1

        assert res1 == [
            APITest(foo="Never", bar=True, baz=5),
            APITest(foo="go", bar=True, baz=2),
            APITest(foo="against", bar=True, baz=7),
            APITest(foo="a", bar=True, baz=1),
            APITest(foo="Sicilian", bar=True, baz=8),
            APITest(foo="when", bar=True, baz=4),
            APITest(foo="death", bar=True, baz=5),
            APITest(foo="is", bar=True, baz=2),
            APITest(foo="on", bar=True, baz=2),
            APITest(foo="the", bar=True, baz=3),
            APITest(foo="line", bar=True, baz=4),
        ]

        res2 = list(self.echoer(data))
        assert len(Cache.db) == 1
        cached = list(Cache.db.values())[0]
        obj = json.loads(cached)
        assert {"foo": "Never", "bar": True, "baz": 5} in obj["data"]

        assert res1 == res2

        Cache.clear()
        assert len(Cache.db) == 0
