import importlib.metadata

from click.testing import CliRunner


class TestCLI:
    def test_base(self, monkeypatch):
        metadata = importlib.metadata.metadata("cliomatic")
        monkeypatch.setattr(
            "importlib.metadata.version",
            lambda module_name: metadata["Version"],
        )

        # late import to give chance to fixtures to monkeypatch what is needed
        from cliomatic_cli import cli

        # _cli = make_cli("cliomatic_cli", "cliomatic_cli")
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["--version"])
        assert result.exit_code == 0
        assert f"version {metadata['Version']}" in result.output

        result = runner.invoke(cli.cli, ["--help"])
        assert result.exit_code == 0
        assert "init" in result.output
