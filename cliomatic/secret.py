from dataclasses import dataclass

import secretstorage

from .logger import logger
from .response import APIGenerator, APIResponse


@dataclass
class SecretGetResponse(APIResponse):
    key: str | None = None
    secret: str | None = None


class Vault:
    def __init__(self, application_key: str):
        try:
            self.conn = secretstorage.dbus_init()
        except secretstorage.exceptions.SecretServiceNotAvailableException:
            logger.warning("SecretStorage is not available")
            self.conn = None
        self.application_key = application_key
        self.prefix = application_key.upper()

    def add(self, key: str, value: bytes):
        """Store a secret under key"""
        if not self.conn:
            return
        coll = secretstorage.collection.get_default_collection(self.conn)
        logger.debug(f"Storing secret for {key}")
        coll.create_item(
            label=f"{self.prefix}.{key}",
            attributes={"application": self.application_key, "key": key},
            secret=value,
            replace=True,
        )

    def remove(self, key: str):
        """Remove key from vault"""
        if not self.conn:
            return
        for item in secretstorage.collection.search_items(
            self.conn, {"application": self.application_key}
        ):
            if item.get_label() == f"{self.prefix}.{key}":
                logger.debug(f"Removing key {key}")
                item.delete()

    def list(self) -> APIGenerator[SecretGetResponse]:
        """List all secrets from collection"""
        if not self.conn:
            yield from []
        logger.debug(f"Looking up secrets in {self.application_key}")
        for item in secretstorage.collection.search_items(
            self.conn, {"application": self.application_key}
        ):
            try:
                secret = item.get_secret().decode("utf8")
            except secretstorage.exceptions.LockedException:
                item.unlock()
                secret = item.get_secret().decode("utf8")
            yield SecretGetResponse(key=item.get_attributes()["key"], secret=secret)

    def asdict(self):
        """Retrieve all secrets from collection, as a dictionary"""
        res = {}

        for secret in self.list():
            node = res
            path = None
            key = secret.key
            label = key.split(".")
            for path in label[:-1]:
                if path not in node:
                    node[path] = {}
                node = node[path]
            node[label[-1]] = secret.secret
        return res
