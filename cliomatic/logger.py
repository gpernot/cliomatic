import logging

FORMAT = "[%(levelname)s] %(filename)s:%(lineno)d %(message)s"
logging.basicConfig(format=FORMAT)

logger = logging.getLogger()
