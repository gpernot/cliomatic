import datetime
import re
from dataclasses import dataclass
from typing import Any, Callable

from .exceptions import CliomaticError
from .response import APIResponse

TOperator = Callable[[Any, str], bool]


@dataclass
class Filter:
    fieldname: str
    operator: TOperator
    operand: str


class FilterRegex:
    re_cache: dict[str, re.Pattern] = {}

    @classmethod
    def test(cls, ref: str, value: str) -> bool:
        regex = cls.re_cache.get(value)
        if not regex:
            regex = re.compile(value)
            cls.re_cache[value] = regex
        return True if regex.match(ref) else False


operators: dict[str, TOperator] = {
    "≅": FilterRegex.test,
    "≥": lambda field, operand: field >= operand,
    "≤": lambda field, operand: field <= operand,
    "≠": lambda field, operand: field != operand,
    "=": lambda field, operand: field == operand,
    ">": lambda field, operand: field > operand,
    "<": lambda field, operand: field < operand,
}

operand_casts: dict[type, Callable[[Any], Any]] = {
    datetime.date: datetime.date.fromisoformat,
    int: int,
    bool: lambda val: val.lower() in ["true", "yes", "on"],
}


def compare(
    operator: TOperator, record: APIResponse, fieldname: str, operand: str
) -> bool:
    field = getattr(record, fieldname)
    if type(field) in operand_casts:
        operand = operand_casts[type(field)](operand)
    return operator(field, operand)


def _clear_operators(spec: str):
    """
    Convert double-char operators to single-char for unambiguous regex parsing.
    e.g.: (?P<operator>>=|>|<|<=) -> (?P<operator>≥|>|<|≤)
    """
    convs = [("~=", "≅"), ("==", "="), (">=", "≥"), ("<=", "≤"), ("!=", "≠")]
    for double, simple in convs:
        if double in spec:
            return spec.replace(double, simple, 1)
    return spec


def parse_filter(spec: str) -> Filter:
    ops = "|".join(operators.keys())
    pattern = rf"(?P<fieldname>\w+)(?P<operator>{ops})(?P<operand>.+)"
    m = re.match(pattern, _clear_operators(spec))
    if m:
        return Filter(m["fieldname"], operators[m["operator"]], m["operand"])
    else:
        raise CliomaticError("Invalid filter [{spec}]")


def filter_match(
    record: APIResponse,
    filters: list[Filter],
) -> bool:
    """
    Check if record matches filters
    """
    # iterate over all filters
    for filter_ in filters:
        if not compare(filter_.operator, record, filter_.fieldname, filter_.operand):
            return False
    return True
