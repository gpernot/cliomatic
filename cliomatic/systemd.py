import os
import socket

from .logger import logger


class Notifier:
    def __init__(self):
        self.socket = socket.socket(family=socket.AF_UNIX, type=socket.SOCK_DGRAM)
        self.address = os.getenv("NOTIFY_SOCKET")
        if self.address is None:
            logger.info("Not started from systemd")

    def _send(self, msg):
        """Send string `msg` as bytes on the notification socket"""
        if self.address is not None:
            self.socket.sendto(msg.encode(), self.address)

    def ready(self):
        """Report ready service state"""
        self._send("READY=1\n")
