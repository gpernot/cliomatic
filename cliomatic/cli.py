import atexit
import importlib
import importlib.metadata
import logging
import pkgutil
import sys
from enum import IntEnum
from typing import Generator

import click

try:
    import icecream  # type: ignore
except ImportError:
    pass
else:
    icecream.install()

from .cache import Cache
from .config import load_defaults
from .filtering import parse_filter
from .logger import logger
from .output import Renderer, RenderFormat

VERSION = "0.2.0"


class LogLevel(IntEnum):
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL


class CLI(click.Group):
    def __init__(self, project_name=None, module_name=None, *args, **kwargs):
        super().__init__(*args, result_callback=self._apiresponse_output, **kwargs)
        self.project_name = project_name
        self.module_name = module_name
        self._load_commands()
        self._load_metadata()
        self.context_settings = load_defaults(project_name)

    def _apiresponse_output(self, result, *args, **kwargs):
        if isinstance(result, Generator):
            renderer = click.get_current_context().obj.renderer
            out = renderer(result)
            click.echo(out)

    def _load_commands(self):
        """Load modules providing CLI commands"""
        commands_module = pkgutil.resolve_name(self.module_name)
        plugindir = commands_module.__path__
        modules = pkgutil.iter_modules(plugindir)
        sys.path.append(plugindir)
        for command in modules:
            # A module is a command if:
            # 1- it is a package (ie, has a __init__.py)
            # 2- has a module_name.command module (ie module_name/command.py) that
            #    exports a cli() and an init() functions
            if not command.ispkg:
                # module command.name is not a package
                continue
            try:
                module = importlib.import_module(
                    f".{command.name}.command", package=self.module_name
                )
                module.init()
            except (ImportError, AttributeError):
                # module command.name is not a command
                pass
            else:
                self.add_command(module.cli, module.cli.name)

    def _load_metadata(self):
        metadata = importlib.metadata.metadata(self.project_name)
        self.help = metadata["Description"]

    def list_commands(self, ctx):
        return list(self.commands)

    def get_command(self, ctx, cmd_name):
        if cmd_name in self.commands:
            return self.commands[cmd_name]


@click.option(
    "--log-level",
    "-l",
    "loglevel",
    default=LogLevel.WARNING.name,
    show_default=True,
    type=click.Choice(list(LogLevel.__members__.keys()), case_sensitive=False),
)
@click.option(
    "--workers",
    "-j",
    default=8,
    help="Number of parallel workers",
    show_default=True,
    type=int,
)
@click.option(
    "--output-format",
    "-o",
    "output",
    default="text",
    help="Output format, when applicable",
    show_default=True,
    type=click.Choice(list(RenderFormat.__members__.keys()), case_sensitive=False),
)
@click.option(
    "--headers/--no-headers",
    "-h/-H",
    help="Whether to show filed headers",
    default=True,
    show_default=True,
)
@click.option("--fields", "-f", help="A coma-separated list of fields to output")
@click.option(
    "--filter",
    "-F",
    "filters",
    help="Filter field according to regexp. Multiple values allowed.",
    multiple=True,
    metavar="<field>=<regexp>",
)
@click.option("--clear-cache", "-C", help="Clear API cache", is_flag=True)
@click.pass_obj
def _cli(
    obj,
    loglevel,
    workers,
    output,
    headers,
    fields,
    filters,
    clear_cache,
    **kwargs,
):
    obj.loglevel = loglevel
    obj.workers = workers

    if fields:
        _fields = fields.split(",")
    else:
        _fields = None

    _filters = [parse_filter(spec) for spec in filters]

    obj.renderer = Renderer(RenderFormat[output], headers, _fields, _filters)

    # add extra parameters to context.obj
    for k, v in kwargs.items():
        setattr(obj, k, v)

    # initialize  logger
    logger.setLevel(LogLevel[loglevel])

    # clear cache, module_name-wise
    if clear_cache:
        Cache.clear()
    atexit.register(Cache.close)


def make_cli(project_name: str, module_name: str, version: str | None = None):
    """Build a click.group cli() for the given cliomatic app"""

    # initialize cache witch proper module_name
    Cache(module_name=module_name)

    group_decorator = click.group(
        cls=CLI, project_name=project_name, module_name=module_name
    )
    if version is None:
        version = importlib.metadata.version(module_name)
    version_option_decorator = click.version_option(version=version)

    return group_decorator(version_option_decorator(_cli))
