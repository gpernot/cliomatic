import functools
import os
import tomllib
from collections import UserDict

from .logger import logger
from .secret import Vault


class ContextObject(UserDict):  # pylint: disable=too-few-public-methods
    """Empty object to host any context configuration data.
    Can be accessed as an object as well as a dict."""

    def __setattr__(self, key, value):
        if key != "data":
            self.__setitem__(key, value)
        else:
            super().__setattr__(key, value)

    def __getattr__(self, key):
        if key != "data":
            if key not in self:
                raise AttributeError
            return self.__getitem__(key)
        else:
            return super().__getattr__(key)

    @classmethod
    def from_dict(cls, orig: dict):
        _self = cls()
        for k, v in orig.items():
            if isinstance(v, dict):
                _self[k] = cls.from_dict(v)
            else:
                _self[k] = v
        return _self


def _update_from_vault(config: dict, vault: dict):
    for k, v in config.items():
        if isinstance(v, dict):
            _update_from_vault(v, vault)
        elif isinstance(v, str):
            if v.startswith("@vault:"):
                try:
                    config[k] = functools.reduce(
                        # get vault[k_1][k_2]...[k_n]
                        lambda _dict, _key: _dict[_key],  # type: ignore
                        [vault] + v[7:].split("."),  # type: ignore
                    )
                except KeyError:
                    logger.warning(f"Key [{v}] is not in vault")


def load_defaults(project_name: str):
    """Load default values for commands from config file and vault."""

    conffile = os.path.expandvars("")
    if "XDG_CONFIG_HOME" in os.environ:
        conffile = os.path.expandvars(f"$XDG_CONFIG_HOME/{project_name}.toml")
    else:
        conffile = os.path.expandvars(f"$HOME/.config/{project_name}.toml")

    # load config file
    try:
        with open(os.path.expanduser(conffile), "rb") as f:
            config = tomllib.load(f)
    except FileNotFoundError:
        logger.warning(f"Config file {conffile} not found")
        config = {}

    # replace references to vault with actual values
    vault = Vault(application_key=project_name)
    _update_from_vault(config, vault.asdict())

    # setup context object as ContextObject hierarchy
    context_object = ContextObject.from_dict(config)

    return {"default_map": config.get("command", {}), "obj": context_object}
