"""
Fixtures to use for testing Cliomatic apps
"""

import dbm
import os

import pytest

from .cache import Cache

fakedbm: dict = {}


def _mock_dbm(dbame: str, mode: str):
    global fakedbm
    if mode == "n":
        fakedbm = {}
    return fakedbm


def _nop(*args, **kwargs):
    pass


@pytest.fixture(autouse=True)
def init_cache(monkeypatch):
    monkeypatch.setattr(dbm, "open", _mock_dbm)
    monkeypatch.setattr(os, "makedirs", _nop)

    Cache(module_name="test", clear=True)
