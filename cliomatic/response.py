import dataclasses
import datetime
import json
from collections.abc import Callable
from functools import wraps
from typing import Generator, TypeVar


@dataclasses.dataclass
class APIResponse:
    pass


T = TypeVar("T", bound=APIResponse)

APIGenerator = Generator[T, None, None]


def typed_apidoc(_type):
    """Decorator for click.commands that adds fields documentation.

    Fields are extracted from given _type.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        fields = [field.name for field in dataclasses.fields(_type)]
        wrapper.__doc__ = f"\n\nOutput fields: {', '.join(fields)}"
        return wrapper

    return decorator


def apidoc(func: Callable[..., APIGenerator[APIResponse] | None]):
    """Decorator for click.commands that adds fields documentation.

    Fields are extracted from command return type annotation (an APIGenerator[APIResponse]).
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    # update docstring if function is properly annotated
    if ret := func.__annotations__.get("return"):
        if ret_type := ret.__args__:
            apiresponse = ret_type[0]
            fields = [field.name for field in dataclasses.fields(apiresponse)]
            wrapper.__doc__ = (
                getattr(wrapper, "__doc__", "")
                + f"\n\nOutput fields: {', '.join(fields)}"
            )
    return wrapper


class APIEncoder(json.JSONEncoder):
    def default(self, o):
        if issubclass(type(o), APIResponse):
            return dataclasses.asdict(o)
        elif issubclass(type(o), datetime.date):
            return o.isoformat()
        return super().default(o)
