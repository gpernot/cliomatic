"""
Various click utility classes
"""

import os.path
import typing as t

from click import Context, File, Option, Parameter, Path, UsageError


class MutuallyExclusiveOption(Option):
    """Mutuallly exclusive group of options"""

    #  https://gist.github.com/jacobtolar/fb80d5552a9a9dfc32b12a829fa21c0#
    def __init__(self, *args, **kwargs):
        self.mutually_exclusive = set(kwargs.pop("mutually_exclusive", []))
        help = kwargs.get("help", "")
        if self.mutually_exclusive:
            ex_str = ", ".join(self.mutually_exclusive)
            kwargs["help"] = help + (
                " NOTE: This argument is mutually exclusive with "
                " arguments: [" + ex_str + "]."
            )
        super(MutuallyExclusiveOption, self).__init__(*args, **kwargs)

    def handle_parse_result(self, ctx, opts, args):
        if self.mutually_exclusive.intersection(opts) and self.name in opts:
            raise UsageError(
                "Illegal usage: `{}` is mutually exclusive with "
                "arguments `{}`.".format(self.name, ", ".join(self.mutually_exclusive))
            )

        return super(MutuallyExclusiveOption, self).handle_parse_result(ctx, opts, args)


class ExpandUserFile(File):
    """Like click.File but do os.expanduser before opening file."""

    def convert(
        self, value: t.Any, param: t.Optional["Parameter"], ctx: t.Optional["Context"]
    ) -> t.Any:
        return super().convert(os.path.expanduser(value), param, ctx)


class ExpandUserPath(Path):
    """Like click.Path but do os.expanduser before opening file."""

    def convert(
        self, value: t.Any, param: t.Optional["Parameter"], ctx: t.Optional["Context"]
    ) -> t.Any:
        return super().convert(os.path.expanduser(value), param, ctx)
