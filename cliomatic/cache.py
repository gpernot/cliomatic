import dbm
import hashlib
import json
import os
import time
from functools import wraps

from .exceptions import CliomaticError
from .logger import logger
from .response import APIEncoder, APIGenerator, APIResponse

ONE_HOUR = 60
ONE_DAY = ONE_HOUR * 24
ONE_WEEK = ONE_DAY * 7
ONE_MONTH = ONE_DAY * 30


def cache(keys=None, ttl=5):
    """
    `keys` are instance attributes that will be taken into account in database key
    `ttl` cache duration, in minutes
    """

    def decorator_cache(func):
        @wraps(func)
        def wrapper_cache(self, *args, **kwargs) -> APIGenerator[APIResponse]:
            db = Cache()

            # compute cache key
            h = hashlib.sha1(usedforsecurity=False)
            h.update(str(func.__qualname__).encode())
            attributes = [getattr(self, key) for key in keys] if keys else []
            h.update(str(attributes).encode())
            params = [f"{k}={v}" for k, v in kwargs.items()]
            h.update(str(params).encode())
            h.update(str(args).encode())
            db_key = h.hexdigest()

            if cached := db.get(db_key):
                # Found in cache
                cache_obj = json.loads(cached)
                if cache_obj["timestamp"] + ttl * 60 > time.time():
                    logger.debug(f"Got {db_key} from cache")
                    # Found and not expired

                    # get APIResonse class from func annotations
                    apiclass = func.__annotations__["return"].__args__[0]

                    # yield results from cache and exit
                    for r in cache_obj["data"]:
                        yield apiclass(**r)
                    return
                else:
                    logger.debug(f"{db_key} has expired from cache")

            # call api, saving results
            res = []
            for r in func(self, *args, **kwargs):
                res.append(r)
                yield r

            # save responses to cache
            cache_obj = {
                "timestamp": time.time(),
                "data": res,
            }
            db[db_key] = json.dumps(cache_obj, cls=APIEncoder)

        return wrapper_cache

    return decorator_cache


class Cache(dict):
    """Singleton cache object. dict-like for mypy sake."""

    db = None
    module_name = None

    def __new__(cls, module_name=None, clear=False):
        if cls.db is None or clear:
            if module_name is None and cls.module_name is None:
                raise CliomaticError("Module name can't be None at initialization")

            if (
                cls.module_name is not None
                and module_name is not None
                and cls.module_name != module_name
            ):
                raise CliomaticError("Renaming module can't occur")

            if module_name:
                # save module_name at first invocation
                cls.module_name = module_name

            if not (cache_db_base_dir := os.getenv("XDG_STATE_HOME")):
                cache_db_base_dir = os.path.join(os.getenv("HOME"), ".local", "state")
            os.makedirs(os.path.join(cache_db_base_dir, cls.module_name), exist_ok=True)

            cls.db = dbm.open(
                os.path.join(cache_db_base_dir, cls.module_name, "cache"),
                "n" if clear else "c",
            )
        return cls.db

    @classmethod
    def clear(cls):
        cls.db = None
        cls(clear=True)

    @classmethod
    def close(cls):
        if cls.db:
            cls.db.close()
