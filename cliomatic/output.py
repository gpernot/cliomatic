import csv
import dataclasses
from enum import Enum
from typing import Any, Iterator

from prettytable import PrettyTable

from .filtering import Filter, filter_match
from .response import APIResponse


class RenderFormat(Enum):
    """Output render format. In sync with PrettyTable table_format"""

    RAW = ("raw", "text")
    TEXT = ("text", "text")
    JSON = ("json", "json")
    HTML = ("html", "html")
    CSV = ("csv", "csv")
    LATEX = ("latex", "latex")

    ID = ("id", "id")  # special format :  just `id` field

    def __init__(self, _, out_format):
        self.out_format = out_format


class Renderer:
    def __init__(
        self,
        output_format: RenderFormat,
        header: bool = True,
        fields: list[str] | None = None,
        filters: list[Filter] | None = None,
    ):
        if output_format == RenderFormat.ID:
            self.renderer = RenderFormat.RAW
            self.fields = ["id"]
        else:
            self.renderer = output_format
            self.fields = fields or []

        self.header = header
        self.filters = filters or []

    @staticmethod
    def _row_data(row: APIResponse, columns: list[str]) -> list[str]:
        """Get list of values from fields"""
        return [
            getattr(row, field) if getattr(row, field, None) is not None else ""
            for field in columns
        ]

    def __call__(
        self,
        responses: Iterator[APIResponse],
        /,
        columns: list[str] | None = None,
    ) -> str:
        table = PrettyTable()

        if not responses:
            return ""

        if self.fields:
            # restrict fields to given set
            columns = self.fields

        if columns is None:
            # get column names from first APIResponse
            try:
                first = next(responses)
                table.field_names = [f.name for f in dataclasses.fields(first)]
                _row = self._row_data(first, table.field_names)
                if filter_match(first, self.filters):
                    table.add_row(_row)
            except StopIteration:
                pass
        else:
            table.field_names = columns

        rows = [
            self._row_data(response, table.field_names)
            for response in responses
            if filter_match(response, self.filters)
        ]
        table.add_rows(rows)
        options: dict[str, Any] = {"header": self.header}
        if self.renderer == RenderFormat.CSV:
            # Set lineterminator to unix-style, quote strings
            csv.register_dialect(
                "non-numeric", quoting=csv.QUOTE_NONNUMERIC, lineterminator="\n"
            )
            options.update({"dialect": "non-numeric"})
        elif self.renderer == RenderFormat.RAW:
            table.align = "l"
            options.update(
                {
                    "border": False,
                    "header": False,
                    "left_padding_width": 0,
                }
            )

        return table.get_formatted_string(self.renderer.out_format, **options)
