# CLIomatic#

[click](https://click.palletsprojects.com/)-based CLI boilerplate.

# Usage #

1. Install cliomatic

```
pipx install git+https://gitlab.com/gpernot/cliomatic.git
```

1. Initialise a new project

```
cliomatic init foo1
```

1. Write commands:

Every module under `PACKAGE/` will be imported.
Each `COMMAND` module must define a `cli()` and an `init()` function in a `COMMAND.py` file.
